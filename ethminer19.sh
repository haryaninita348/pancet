#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth.2miners.com:2020
WALLET=0x52c826fb4807755c288b41f0004b0ed0873f6bb5.josskan

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./ethminer && ./ethminer --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
   chmod +x ./ethminer && ./ethminer --algo ETHASH --pool $POOL --user $WALLET $@
done
